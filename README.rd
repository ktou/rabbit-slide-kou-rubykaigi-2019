= Better CSV processing with Ruby 2.6

csv, one of the standard libraries, in Ruby 2.6 has many improvements:

  * Default gemified
  * Faster CSV parsing
  * Faster CSV writing
  * Clean new CSV parser implementation for further improvements
  * Reconstructed test suites for further improvements
  * Benchmark suites for further performance improvements

These improvements are done without breaking backward compatibility.

This talk describes details of these improvements by a new csv maintainer.

== License

=== Slide

CC BY-SA 4.0

Use the followings for notation of the author:

  * Kouhei Sutou

==== ClearCode Inc. logo

CC BY-SA 4.0

Author: ClearCode Inc.

It is used in page header and some pages in the slide.

== For author

=== Show

  rake

=== Publish

  rake publish

== For viewers

=== Install

  gem install rabbit-slide-kou-rubykaigi-2019

=== Show

  rabbit rabbit-slide-kou-rubykaigi-2019.gem
