#!/usr/bin/env ruby

require "csv"
require "fastest-csv"
require "benchmark"

measure = Benchmark.measure do
  CSV.foreach("KEN_ALL.CSV", "r:cp932", quote_char: nil, strip: '"') do |row|
  end
end
puts(measure)

measure = Benchmark.measure do
  CSV.foreach("KEN_ALL_NO_QUOTE.CSV", "r:cp932", quote_char: nil) do |row|
  end
end
puts(measure)

measure = Benchmark.measure do
  File.open("KEN_ALL.CSV", "r:cp932") do |file|
    file.each_line(chomp: true) do |line|
      line.split(",")
    end
  end
end
puts(measure)

measure = Benchmark.measure do
  FastestCSV.open("KEN_ALL.CSV", "r:cp932") do |csv|
    csv.each do |row|
    end
  end
end
puts(measure)
