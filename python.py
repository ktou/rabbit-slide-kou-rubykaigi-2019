#!/usr/bin/env python

import codecs
import csv
import time
import pandas as pd

start = time.time()
with codecs.open("KEN_ALL.CSV", "r", "cp932") as input:
    for row in csv.reader(input):
        pass
elapsed_time = time.time() - start
print(elapsed_time)

start = time.time()
with codecs.open("KEN_ALL.CSV", "r", "cp932") as input:
    pd.read_csv(input)
elapsed_time = time.time() - start
print(elapsed_time)
